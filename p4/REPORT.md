# P4: Mandelbrot

Tiempo secuencial=30.424447  

Tiempo paralelo=19.510083  

Speedup=30.424447/19.510083=1.55942

## Ejercicio 1 
__1.	Explica brevemente la función que desarrolla cada una de las directivas y funciones de OpenMP que has utilizado
(sólo las que no hayas explicado en prácticas anteriores).__  

#pragma omp critical  
Esta función sirve para iniciar una sección crítica

__2.	Etiqueta todas las variables y explica por qué le has asignado esa etiqueta.__  

#pragma omp parallel private(i,j) shared(x,y,c_max,r,g,b,count)  

Las variables i,j las pongo en privadas ya que cada hilo va a usarlas individualmente para los for y 
las variables x,y,c_max,r,g,b,count son compartidas porque son datos globales para todos los hilos.  

__3.	Explica brevemente cómo se realiza el paralelismo en este programa, indicando las partes que son secuenciales y las paralelas.__  

Las partes secuenciales del programa son las de los métodos siguientes al explode, que son ppma_write,ppma_write_data…  

La parte paralela es en el main donde ejecutas varios bucles for, cada uno de ellos con un #pragma omp for, 
todos ellos englobados por un #pragma omp parallel private(i,j) shared(x,y,c_max,r,g,b,count).  

