# P2: MatMul

##### Para las ejecuciones uso una matriz de:1060x1060

## Ejercicio 1

##### Tiempo ejecución sin paralelizar=9.933038

##### Tiempo ejecución paralizado=2.931745

__1.	Explica brevemente la función que desarrolla cada una de las directivas y funciones de OpenMP que has utilizado.__

* #pragma omp parallel private(i,j,k) shared(A, B, C, dim):

Con esta función indico que voy a paralelizar la función matmul poniendo las variables i,j,k que se usan para los bucles for en privado y las variables A,B,C y dim en compartidas entre los hilos.
* #pragma omp for:

Con esta función paralelizo el for entre los distintos hilos.

__2.	Etiqueta (OpenMP) todas las variables y explica por qué le has asignado esa etiqueta.__
* #pragma omp parallel private(i,j,k) shared(A, B, C, dim)

Las variables i,j,k las defino como privadas ya que son las que usa cada hilo dentro de la región paralela para los for. Por otra parte tengo las variables A,B,C y dim compartidas porque todos los hilos trabajan con ellas.

__3.	Explica cómo se reparte el trabajo entre los threads del equipo en el código paralelo.__  

El trabajo se dividirá en partes iguales para los threads siempre que se pueda  

__4.	Calcula la ganancia (speedup) obtenido con la paralelización.__  

SpeedUp=Tiempo ejecución secuencial/Tiempo ejecución paralela --> 9.933038/2.931745=3.38809

![speedup](p2/graficas/graficaSpeedup.png)

##### Benchmarking

    #!/usr/bin/env bash
    file=resultadosPractica2.log
    touch $file

    tamanio=1060
    
    //tendré un matmulPar y matmulSeq
    g++ -std=c++17 -pthread -o matmulPar matmulPar.c -fopenmp
    g++ -std=c++17 -pthread -o matmulSeq matmulSeq.c -fopenmp

    echo "Comienzo benchmarking"  
    for season in 1 2; do  
        for benchcase in Par Sec; do  
            echo $benchcase >> $file  
            for i in `seq 1 1 10`;  
            do  
                echo $i"=" >> $file # iteration  
                ./matmul$benchcase $tamanho >> $file # results dumped  
                sleep 1  
            done  
        done  
    done  



## Ejercicio 2
###### Tiempo ejecución:
-	Secuencial: 3.473865 
-	Paralelo sin Schedule: 1.502219 --> Ganancia: 3.473865 /1.502219 =2.312489
-	Static: 1.597237 --> Ganancia: 3.473865 /1.597237=2.174921
-	Dynamic: 0.870497 --> Ganancia: 3.473865 /0.870497 =3.99066
-	Guided: 1.544911 --> Ganancia: 3.473865 /1.544911=2.24858

![tiempos](p2/graficas/tiemposEjecucion.png)

__1.	¿Qué diferencias observas con el ejercicio anterior en cuanto a la ganancia obtenida sin la cláusula schedule? Explica con detalle a qué se debe esta diferencia.__  

En el ejercicio anterior la ganancia obtenida con la paralelización era mayor que en este ejercicio con el schedule.  

__2.	¿Cuál de los tres algoritmos de equilibrio de carga obtiene mejor resultado? Explica por qué ocurre esto, en función de cómo se reparte la carga de trabajo y las operaciones que tiene que realizar cada thread.__  

El algoritmo que obtiene el mejor resultado es el Dynamic. 
Esto ocurre debido a que el Dynamic debe repartir mejor entre los thread el tamaño del trabajo a realizar por cada uno de ellos.  

__3.	Para el algoritmo static piensa cuál será el tamaño del bloque óptimo, en función de cómo se reparte la carga de trabajo.__

Para el algoritmo static el tamaño de bloque óptimo será de número de iteraciones/número de threads ya que todos tienen la misma cantidad de trabajo.  

__4.	Para el algoritmo dynamic determina experimentalmente el tamaño del bloque óptimo. 
Para ello mide el tiempo de respuesta y speedup obtenidos para al menos 10 tamaños de bloque diferentes. 
Presenta una tabla resumen de los resultados y explicar detalladamente estos resultados.__  

##### script:

    #!/usr/bin/env bash
    file=resultsDynamic.log
    touch $file
    for benchcase in 1 11 21 31 41 51 61 71 81 91; do
        echo "Tamaño dynamic = $benchcase" >> $file
        for i in `seq 1 1 10`;
        do
            printf "$i:" >> $file # iteration
            ./matmul $benchcase >> $file # results dumped
            sleep 1
        done
    done


Iteraciones por bloques:  

-	Tamaño= 1
Tiempo=0.870497
-	Tamaño= 11
Tiempo=0.883317
-	Tamaño= 21
Tiempo=0.859019
-	Tamaño= 31
Tiempo=0.861066
-	Tamaño= 41
Tiempo=0.890501
-	Tamaño= 51
Tiempo=0.877879
-	Tamaño= 61
Tiempo=0.886827
-	Tamaño= 71
Tiempo=0.947246
-	Tamaño= 81
Tiempo=0.924443
-	Tamaño= 91
Tiempo=0.967839

![tiemposDynamic](p2/graficas/tiemposDynamic.png)

Como podemos observar en la gráfica a medida que el tamaño introducido en el dynamic aumenta
el tiempo de ejecución va aumentando también.

__5.	Explica los resultados del algoritmo guided, en función del reparto de carga de trabajo que realiza.__  

Este algoritmo funciona de manera que cada hilo ejecuta una parte de las iteraciones
pidiendo despúes otra parte. Al principio las partes son grandes y luego
se irán reduciendo hasta el parámetro que hayas indicado.

## Ejercicio 3

__1.	¿Cómo se comportará? Compáralo con el ejercicio anterior.__  

Se comportará de manera similar al anterior ejercicio ya que es multiplicar la mitad de la matriz también, pero en vez de la superior la inferior.  

__2.	¿Cuál crees que es el mejor algoritmo de equilibrio en este caso? Explica las diferencias que ves con el ejercicio anterior.__  

La diferencia con el ejercicio anterior es que al principio no hay una carga de trabajo muy grande.
Para mi el algoritmo Dynamic seguirá siendo el mejor para este caso.  


__3.	¿Cuál es el peor algoritmo para este caso?__  

El peor algoritmo es el static ya que al repartir el trabajo en bloques iguales va a haber unos con mucho trabajo y otros con poco.  

