#include <stdio.h>
#include <malloc.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <sys/times.h>

//paralela
#include <omp.h>
#define omp_get_num_threads() 4

#define RAND rand() % 100

void init_mat_sup (int dim, float *M);
void init_mat_inf (int dim, float *M);
void matmul (float *A, float *B, float *C, int dim);
void matmulPar (float *A, float *B, float *C, int dim);
void matmul_sup (float *A, float *B, float *C, int dim);
void matmul_supPar (float *A, float *B, float *C, int dim,int block_size);
void matmul_inf ();
void print_matrix (float *M, int dim);

/* Usage: ./matmul <dim> [block_size]*/

int main (int argc, char* argv[])
{
	int block_size = 1, dim;
	float *A, *B, *C;

   dim = atoi (argv[1]);
   if (argc == 3) block_size = atoi (argv[2]);


	int tamanho=dim*dim;
 	A=(float*)malloc(tamanho*sizeof(float));
	B=(float*)malloc(tamanho*sizeof(float));
	C=(float*)malloc(tamanho*sizeof(float));

	init_mat_sup(dim,A);
	init_mat_sup(dim,B);
	init_mat_sup(dim,C);

	double tiempo_comienzo = omp_get_wtime();
	matmul(A,B,C,dim);
	printf("Multiplicacion normal\n");
	//print_matrix(C,dim);
	double tiempo_total = omp_get_wtime() - tiempo_comienzo;
	printf("Tiempo matmul: %f\n",tiempo_total);

	tiempo_comienzo = omp_get_wtime();
	matmulPar(A,B,C,dim);
	printf("Multiplicacion paralela\n");
	//print_matrix(C,dim);
	tiempo_total = omp_get_wtime() - tiempo_comienzo;
	printf("Tiempo matmul Paralela: %f\n",tiempo_total);

	tiempo_comienzo = omp_get_wtime();
	printf("Multiplicacion sup\n");
	matmul_sup(A,B,C,dim);
	//print_matrix(C,dim);
	tiempo_total = omp_get_wtime() - tiempo_comienzo;
	printf("Tiempo matmul_sup: %f\n",tiempo_total);
	
	// tiempo_comienzo = omp_get_wtime();
	// printf("Multiplicacion sup Paralela\n");
	// matmul_supPar(A,B,C,dim,NULL);
	// //print_matrix(C,dim);
	// tiempo_total = omp_get_wtime() - tiempo_comienzo;
	// printf("Tiempo matmul_supPar: %f\n",tiempo_total);

	//bucle para probar el dynamic con varios tamaños distintos
	for(int i=1;i<=101;i+=10){
		tiempo_comienzo = omp_get_wtime();
		printf("Multiplicacion sup dynamic Paralela tamaño= %d\n",i);
		matmul_supPar(A,B,C,dim,i);
		tiempo_total = omp_get_wtime() - tiempo_comienzo;
		printf("Tiempo matmul_supPar: %f\n",tiempo_total);
	}
	exit (0);
}

// 0 0 0
// 0 0 1
// 0 1 1
void init_mat_sup (int dim, float *M)
{
	int i,j,m,n,k;

	for (i = 0; i < dim; i++) {
		for (j = 0; j < dim; j++) {
			if (j <= i)
				M[i*dim+j] = 0.0;
			else
				M[i*dim+j] = RAND;
		}
	}
}

// 1 1 0
// 1 0 0
// 0 0 0
void init_mat_inf (int dim, float *M)
{
	int i,j,m,n,k;

	for (i = 0; i < dim; i++) {
		for (j = 0; j < dim; j++) {
			if (j >= i)
				M[i*dim+j] = 0.0;
			else
				M[i*dim+j] = RAND;
		}
	}
}

//PARALELIZAR

void matmul (float *A, float *B, float *C, int dim)
{
	int i, j, k;


	for (i=0; i < dim; i++)
		for (j=0; j < dim; j++)
			C[i*dim+j] = 0.0;

	for (i=0; i < dim; i++)
		for (j=0; j < dim; j++)
			for (k=0; k < dim; k++)
				C[i*dim+j] += A[i*dim+k] * B[j+k*dim];

}

void matmulPar (float *A, float *B, float *C, int dim)
{
	int i, j, k;
	#pragma omp parallel private(i,j,k) shared(A, B, C, dim)
	{

	#pragma omp for
	for (i=0; i < dim; i++)
		for (j=0; j < dim; j++)
			C[i*dim+j] = 0.0;

	#pragma omp for
	for (i=0; i < dim; i++)
		for (j=0; j < dim; j++)
			for (k=0; k < dim; k++)
				C[i*dim+j] += A[i*dim+k] * B[j+k*dim];
	}
}


void matmul_sup (float *A, float *B, float *C, int dim)
{
	int i, j, k;
	for (i=0; i < dim; i++)
		for (j=0; j < dim; j++)
			C[i*dim+j] = 0.0;

	for (i=0; i < (dim-1); i++)
		for (j=0; j < (dim-1); j++)
			for (k=(i+1); k < dim; k++)
				C[i*dim+j] += A[i*dim+k] * B[j+k*dim];
}

void matmul_supPar (float *A, float *B, float *C, int dim,int block_size)
{
	int i, j, k;
	#pragma omp parallel private(i,j,k) shared(A, B, C, dim)
	{
	#pragma omp for schedule(dynamic,block_size)
	for (i=0; i < dim; i++)
		for (j=0; j < dim; j++)
			C[i*dim+j] = 0.0;

	#pragma omp for schedule(dynamic,block_size)
	for (i=0; i < (dim-1); i++)
		for (j=0; j < (dim-1); j++)
			for (k=(i+1); k < dim; k++)
				C[i*dim+j] += A[i*dim+k] * B[j+k*dim];
	}
}

void matmul_inf (float *A, float *B, float *C, int dim)
{
	int i, j, k;

	for (i=0; i < dim; i++)
		for (j=0; j < dim; j++)
			C[i*dim+j] = 0.0;

	for (i=1; i < dim; i++)
		for (j=1; j < dim; j++)
			for (k=0; k < i; k++)
				C[i*dim+j] += A[i*dim+k] * B[j+k*dim];
}

void print_matrix (float *M, int dim){
	int i, j, k;

	for (i=0; i < dim; i++)
		for (j=0; j < dim; j++)
			printf("%f\n",M[i,j]);
}
