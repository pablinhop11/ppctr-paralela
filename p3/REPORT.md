# P3: VideoTask
__1.Explica que funcionalidad has tenido que añadir/modificar sobre el código original. 
Explica si has encontrado algún fallo en la funcionalidad (“lo que hace el programa”, independientemente al paralelismo) y cómo lo has corregido. 
Atención: encontrar el fallo es algo “para nota”, por lo que no te deberías preocupar si no lo encuentras.__ 

La función que estaba mal en el código original es la del filtrado(do while).
El fallo que encontré fue el de que al ejecutarlo sólo se usaba un píxel (el pixel 0). 
Para arreglar esto he introducido varios for para que use todos los pixeles.  

__2.Explica brevemente la función que desarrolla cada una de las directivas y funciones de OpenMP que has utilizado, 
salvo las que ya hayas explicado en la práctica anterior (por ejemplo parallel).__ 

- #pragma omp task shared(i)

Con esta función indico que voy usar una task para paralelizar el programa a 
base de generar tareas.

- #pragma omp taskwait

Con esta función espero a que se haya acabado cada tarea de ejecutar

__3.Etiqueta todas las variables y explica por qué le has asignado esa etiqueta.__  

- #pragma omp task shared(i)

    La variable i es compartida debido a que se usa por todos los hilos.

__4.Explica cómo se consigue el paralelismo en este programa (aplicable sobre la función main).__  

Se consigue paralelismo realizando una paralelización dentro del bucle do while que es el que 
se encarga de filtrar los pixels de cada imagen. Mediante el task hacemos
que cada pixel espere a que se haya filtrado el anterior y con el taskwait
a que se hayan acabado de filtrar todos.

__5.Calcula la ganancia (speedup) obtenido con la paralelización y explica los resultados obtenidos.__  

- Tiempo secuencial=3.163793
- Tiempo paralelo=2.449000
- Speedup=Tiempo secuencial/Tiempo paralelo=3.163793/2.449000=1.291871

##### Benchmarking

    #!/usr/bin/env bash
    file=resultadosPractica3.log

    touch $file
    g++ -std=c++17 -pthread -o video_task video_task.c -fopenmp
    
    echo "Comienzo benchmarking"
    
    for i in `seq 1 1 10`; do
        echo $i"=" >> $file
        ./video_task >> $file
    done


__6.¿Se te ocurre alguna optimización sobre la función fgauss? Explica que has hecho y que ganancia supone con respecto a la versión paralela anterior.__  

- Tiempo optimizando función fgauss=1.062130
- Ganancia=2.449000/1.062130=2.305744

Para optimizar esta función lo que he hecho ha sido un #pragma omp parallel for dentro de la función fgauss poniendo las variables pixels,filtered,heigh, width,filter
compartidas y x,y,sum,dx,dy privadas.