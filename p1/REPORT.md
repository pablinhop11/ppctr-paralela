# P1: Multithreading en C++

## Preguntas

__1. Indica el tiempo que has necesitado para realizar la práctica y el tiempo invertido
en cada una de las secciones de la rúbrica.__  

Para realizar la práctica he tenido que bastante tiempo ya que he tenido que recordar
como se usaba c y aprender a usar algunas cosas de c++ como el uso de los threads.

__2. Indica qué has conseguido y qué te falta por cada apartado de la rúbrica. Además, describe las dificultades encontradas durante dicho proceso.__  

De los apartados de la rúbrica he conseguido:
1. La reducción paralela.  

Las dificultades de esta parte fueron las de pensar como usar los threads de la mejor manera
para que el programa funcionase correctamente.

2. El correcto particionado

La dificultad que he tenido en esta parte es la de que al principio pensé en
usar el último hilo con lo que quedara de array si la división del tamaño entre los bloques no era exacta
,pero después pensé que sería mas fácil añadirlo al primer thread y ya el resto de la misma cantidad de trabajo.

3. Los resultados,analisis y evaluación de rendimientos.

__3. Explica el funcionamiento del programa con mayor detalle que lo indicado en el
enunciado, haciendo especial hincapié en el paralelismo y las sincronizaciones.__  

El programa se basa en dos operaciones sum y xor y un main que crea distintos hilos que se encargan de ejecutar una de las dos operaciones.
El main se encarga de comprobar si el programa se va a ejecutar single o multi thread y con esto crea los respectivos threads necesarios,
se crea el array y se rellena. Cuando se usa multi thread debemos calcular el tamaño de trabajo de cada thread y para ello hacemos el particionado (explico en el siguiente ejercicio).
Cada thread realiza la operación correspondiente y añade el resultado a una variable global que se va añadiendo a una propia del main y que tendrá el resultado final.
Para usar ambas operaciones he usado un mutex el cual se bloquea cuando se usa una función y se desbloquea al acabar de usarla.

__4. (Base) Explica el particionado aplicado, qué variaciones se te ocurren y posibles
efectos.__  

Para dividir la carga de trabajo entro los hilos divido el tamaño total del array y cada hilo se encarga de esa cantidad excepto el primero
que se encarga además del resto de la división.

__5. (Base) Explica cómo has hecho los benchmarks, qué metodología de medición has
usado (ROI+gettimeofday, externo), proporciona los resultados (recomendable gráfica) y haz un análisis de los mismos.__  

He ido probando el programa con distintos tamaños para single o multi thread, pero me daba similar ya que me falta quitar los mutex y 
añadir un método para que cada hilo que acabe guarde ahí su suma o xor parcial y que este método lo vaya añadiendo a la final.

Los tiempos secuenciales son:

- Suma según tamaño:

10=0,000397 seg  
100=0,000421 seg  
1000=0,000594 seg  
10000=0,000611 seg  
100000=0,001529 seg  
1000000=0,01151 seg  
10000000=0,130187 seg  
100000000=1,006576 seg  

- Xor según tamaño:


10=0,000375 seg  
100=0,000457 seg  
1000=0,000483 seg  
10000=0,000502 seg  
100000=0,001371 seg  
1000000=0,007698 seg  
10000000=0,103443 seg  
100000000=0,681134 seg  

![tiempos](p1/graficas/tiemposSecuencial.png)

El método xor a medida que aumentamos el tamaño vemos que no tarda tanto como el método de la suma.

__6. (Conector) Explica cómo has hecho los benchmarks de overhead, proporciona los
resultados (recomendable gráfica) y haz un análisis de los mismos.__  

__7. (Mantenibilidad) Explica qué has refactorizado y cómo ha mejorado la mantenibilidad.__  