#include <stdio.h>
#include <thread>
#include <string.h>
#include <iostream>
#include <numeric>
#include <mutex>
#include <omp.h>

std::mutex mtx;

void xorOp(double* array,int arrayLength,int numHilo);
void sum(double* array,int arrayLength,int numHilo);

double sumaPartes;
long xorPartes;

int main(int argc, const char* argv[])
//int arrayLength, char operacion[], int numeroThreads
{
	double tiempo_comienzo = omp_get_wtime();

	printf("Comienza el main\n");

	int arrayLength=atoi(argv[1]);
	const char* operacion=argv[2];
  	int numeroThreads=1;
	double suma;
	long xorT;

	printf("Compruebo si es single o multi\n");
	if (argc<3) {
		printf("Has añadido mal los parámetros\n");
		return 0;
	}
	if(argc==4){
			numeroThreads=atoi(argv[3]);
	}

	printf("Comienza el programa\n");

	//creo el array
	printf("Creo el array\n");
	double* array=(double*)malloc(arrayLength*sizeof(double));
	for(int i=0;i<arrayLength;i++){
		array[i]=(double)i;
	}

	std::thread hilos[numeroThreads];
	int tamanioHilo=arrayLength/numeroThreads;
	int tamanioSobrante=arrayLength-(tamanioHilo*numeroThreads);
	int comienzoSigHilo=tamanioHilo+tamanioSobrante;

		printf("Miro si hago sum o xor\n");
		if(operacion==std::string("sum")){
			for(int i=0;i<numeroThreads;i++){
				if(i==0){
					hilos[i]=std::thread (sum,&array[i],comienzoSigHilo,i);
				}else{
					hilos[i]=std::thread (sum,&array[comienzoSigHilo],tamanioHilo,i);
					comienzoSigHilo+=tamanioHilo;
				}
			}
			for (auto& th : hilos) th.join();

			suma=sumaPartes;
			printf("suma= %f \n",suma);
		}
		else{
			for(int i=0;i<numeroThreads;i++) {
				if(i==0){
						hilos[i]=std::thread(xorOp,&array[i],comienzoSigHilo,i);
				}else{
						hilos[i]=std::thread(xorOp,&array[comienzoSigHilo],tamanioHilo,i);
						comienzoSigHilo+=tamanioHilo;
				}
			}
			for (auto& th :hilos) th.join();

			xorT=xorPartes;
			printf("xor=%ld\n",xorT);
		}
		double tiempo_total = omp_get_wtime() - tiempo_comienzo;
		printf("Tiempo: %f\n",tiempo_total);
}

void sum(double *array, int tamanio,int numHilo){
	mtx.lock();
	double suma=0;
	for (int i;i<tamanio;i++) {
		suma=suma+array[i];
	}
	printf("Hilo: %d,sumo: %f \n",numHilo,suma);
	sumaPartes+=suma;
	mtx.unlock();
}

//explicacion logger
//main genera sus hilos: w1 y w2 que sacan su valores 3 y 2
//ademas tenemos el logger, cuando acaba w2 pasa el resultado a logger y el 1 tambien
//al acabar todos el logger muestra por terminal los resultados de cada uno, los suma
//se compara con la suma de w1 y w2 y retorna
void xorOp(double *array,int tamanio,int numHilo){
	mtx.lock();
	long xorOp=(long)array[0];
	for(int i=1;i<tamanio;i++){
		xorOp=xorOp^(long)array[i];
	}
	printf("Hilo: %d,xor: %ld \n",numHilo,xorOp);
	xorPartes=xorOp;
	mtx.unlock();
}
